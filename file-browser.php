<?php
/**
 * This is the plugin bootstrap file.
 *
 * This file is read by WordPress to generate the plugin information in the
 * plugin admin area. This file also includes call for all of the 
 * dependencies used by the plugin, registers the activation and 
 * deactivation functions, and defines constants as well as a function 
 * (exec_WP_ActiveDirectory) that starts the plugin.
 * 
 * @author      Marius Cucuruz
 * @copyright   Marius Cucuruz, 2018
 * @version     0.1
 *
 * @package     WP_NetworkShare_Browser
 * 
 */

define( 'WPNTFS__PLUGIN_NAME',     'Network Share Browser' );
define( 'WPNTFS__PLUGIN_ID',       'wp-ntfs-browser' );
define( 'WPNTFS__PLUGIN_VERSION',  '0.1' );
define( 'WPNTFS__PLUGIN_PREFIX',   'wp_ntfs_browser_' );
#define( 'WPNTFS__PLUGIN_DIR',      plugin_dir_path( __FILE__ ) );
#define( 'WPNTFS__PLUGIN_URL',      plugins_url('/wp-ntfs-browser/') );
define( 'WPNTFS__PLUGIN_CONFIG_FIELDS', array (
                        # name => title
                        WPNTFS__PLUGIN_PREFIX ."sharePath"          => "Path to Network Share",
                        WPNTFS__PLUGIN_PREFIX ."showHiddenFiles"    => "Show / hide hidden files",
                        WPNTFS__PLUGIN_PREFIX ."showHiddenDirs"     => "Show / hide hidden directories",
                        WPNTFS__PLUGIN_PREFIX ."showThumbnails"     => "Show / hide thumbnails?",
                        WPNTFS__PLUGIN_PREFIX ."showOnlyExt"        => "Only show these extenstions",
                        WPNTFS__PLUGIN_PREFIX ."hideExt"            => "Hide these extenstions",
                        WPNTFS__PLUGIN_PREFIX ."showPath"           => "Show Paths to Users",
                        WPNTFS__PLUGIN_PREFIX ."timeFormat"         => "Timestamp format",
                        )
                    );

$wpNetworkRoot  = '/var/www';
$wpReqPathURL   = (isset($_GET['path']) && $_GET['path'] != DIRECTORY_SEPARATOR) ? $_GET['path'] : NULL;

require_once ('file-browser/class.NTFS_Browser.php');

$browserObj = new WP_NTFS_Browser($wpReqPathURL);
if ($browserObj->isDir())
{
    echo $browserObj->listDirContents();
}
else if ($browserObj->isFile() ){
    echo $browserObj->getFileContents();
}
else {
    echo "<p>Cannot read ". ($wpReqPathURL ?: 'root') .".</p>";
}
