<?php
#namespace WP_NTFS_Browser;

// Abort if this file is accessed directly
if ( ! defined( 'WPINC' ) )
    die;

/**
 * @author      Marius Cucuruz
 * @copyright   Marius Cucuruz, 2018
 * @version     0.1
 *
 * @package     WP_NTFS_Browser
 * 
 * Functionality for browsing a Network Share; if share
 * is M$ WIndows OS then that'll need to be mounted
 * before able to browse.
 * 
 */

# prevent naming collision:
if ( !class_exists( 'WP_NTFS_Browser' ) ) {

  class WP_NTFS_Browser {

    // private variable to hold te LDAP handle
    private $ad_connection;

    /**
     * The loader that's responsible for maintaining and 
     * registering all hooks that power the plugin.
     * @since    0.1
     * @access   protected
     * @var      Plugin_Name_Loader    $loader    Maintains and registers all hooks for the plugin.
     */
    protected $plugin_loader;

    /**
     * The unique identifier of this plugin.
     * @since    0.1
     * @access   protected
     */
    protected $plugin_name = WPNTFS__PLUGIN_ID;
    /**
     * The current version of the plugin.
     * @since    0.1
     * @access   protected
     */
    protected $version = WPNTFS__PLUGIN_VERSION;

    // header content-type for allowed file types that can be opened in browser
    private $headerMIMEs = array(
                            'pdf'       => 'application/pdf',
                            'txt'       => 'text/plain',
                            'html'      => 'text/html',
                            # M$ Office mime types:
                            #'php'       => 'text/plain',
                            'doc'       => 'application/msword',
                            'docx'      => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                            'xls'       => 'application/vnd.ms-excel',
                            'xlsx'      => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                            'ppt'       => 'application/vnd.ms-powerpoint',
                            'pps'       => 'application/vnd.ms-powerpoint',
                            # image mime types:
                            'jpeg'      => 'image/jpeg',
                            'jpg'       => 'image/jpeg',
                            'gif'       => 'image/gif',
                            'svg'       => 'image/svg+xml',
                            'tif'       => 'image/tiff',
                            'tiff'      => 'image/tiff',
                            'ico'       => 'image/ico',
                            'pcx'       => 'image/pcx',
                            'png'       => 'image/png',
                            'bmp'       => 'image/bmp',
                            # miscellaneous:
                            'css'       => 'text/css',
                            'js'        => 'application/javascript',
                            );

    // whether to show hidden files:
    private $showHiddenFiles = FALSE;

    // whether to show hidden directories:
    private $showHiddenDirs = FALSE;

    // whether to show hidden thumbnails:
    private $showThumbnails = FALSE;

    // the ONLY file extensions to show:
    private $showOnlyExt = array();

    // file extensions to HIDE by default:
    private $hideFileExt = array();

    private $showPath    = FALSE;

    private $timeFormat  = 'H:i:s, d-m-Y';

    // system related directory separator:
    private $dirSeparator = DIRECTORY_SEPARATOR;

    // path to action on
    private $resourcePath;

    // URL of path (shown in browser)
    private $resourceURL;

    private $resourceRoot;
    private $isWinFS = false;

    /**
     * Define the core functionality of the plugin.
     *
     * Set the plugin name and version, load dependencies, 
     * define the locale, and set the hooks for the admin
     * and public side of the site.
     */
    public function __construct ($wpGetPathURL = '', $wpSearchKeyword = '', $isWinFS = false) {

        $this->plugin_name      = WPNTFS__PLUGIN_ID;
        $this->load_dependencies();
        $this->define_admin_hooks();
        $this->define_public_hooks();
        #$this->set_locale();
        $this->setDirSeparator(); # set DIR separator ($isWinFS?)

        # get user defined options from WP:
        $this->resourceRoot     = get_option(WPNTFS__PLUGIN_PREFIX ."resourceRoot", "/");#  root location
        $this->sharePath        = get_option(WPNTFS__PLUGIN_PREFIX ."sharePath", "");
            $this->sharePath    = str_ireplace('./', '', $this->sharePath);
        $this->showHiddenFiles  = get_option(WPNTFS__PLUGIN_PREFIX ."showHiddenFiles", 0);
        $this->showHiddenDirs   = get_option(WPNTFS__PLUGIN_PREFIX ."showHiddenDirs", 0);
        $this->showThumbnails   = get_option(WPNTFS__PLUGIN_PREFIX ."showThumbnails", 0);
        $this->hideExt          = get_option(WPNTFS__PLUGIN_PREFIX ."hideExt", 0); # whe
        $this->showPath         = get_option(WPNTFS__PLUGIN_PREFIX ."showPath", 0);
        $this->timeFormat       = get_option(WPNTFS__PLUGIN_PREFIX ."timeFormat", "");

        # handle extensions list to show:
        $this->showOnlyExt      = get_option(WPNTFS__PLUGIN_PREFIX ."showOnlyExt", "");
            $this->showOnlyExt  = explode(',', $this->showOnlyExt); # make array
            $this->showOnlyExt  = array_map('trim', $this->showOnlyExt); # trim values

        # handle extensions list to hide:
        $this->hideFileExt      = get_option(WPNTFS__PLUGIN_PREFIX ."hideFileExt", "")
                                  # apend ext hidden by default:
                                  .('., .., php, env, inc, bak, htaccess, htpasswd');
            $this->hideFileExt  = explode(',', $this->hideFileExt); # make array
            $this->hideFileExt  = array_map('trim', $this->hideFileExt); # trim values


        $this->searchKeyword    = $wpSearchKeyword; # keyword / search term
        $this->resourceURL      = ($wpGetPathURL ?: @$_GET['path']); # get resource URL from user browsing
        $this->resourcePath     = $this->resourceRoot . $this->dirSeparator . $this->sharePath . $this->resourceURL; # set resource PATH
            # remove doubled separators:
            $this->resourcePath = str_ireplace($this->dirSeparator.$this->dirSeparator, $this->dirSeparator, $this->resourcePath);


        /**
         * The `init` hook is useful for intercepting $_GET or $_POST triggers.
         * Fires after WordPress has finished loading but before any headers are sent.
         * Most of WP is loaded at this stage, and the user is authenticated.
         */
        $this->plugin_loader->add_action('init', $this, 'wp_init');

        /**
         * Hook into WP before any contents is generated or sent to the client, 
         * and verify the user is authenticated via AD.
         * 
         * `wp_loaded` action hook is fired once WordPress, all plugins, and 
         * the theme are fully loaded and instantiated;
         * 
         * other options: init, wp_loaded, parse_request, pre_get_posts, get_header etc.
         */
        $this->plugin_loader->add_action('wp_loaded', $this, 'hook_wp_init' );

    }

    /**
     * This is called upon `init` - a hook useful for 
     * intercepting $_GET or $_POST triggers which 
     * fires after WordPress has finished loading 
     * but before any headers are sent.
     * 
     * Most of WP is loaded at this stage and 
     * the user is authenticated.
     */
    public function wp_init () {
        # ensure session is started:
        if (!session_id())
            session_start();

        # capture file view / download requests here, before headers been sent by WP
        if (!$this->isDir($this->resourcePath) && $this->isFile($this->resourcePath))
            die ($this->getFileContents($this->resourcePath));
            #echo ('##['.$this->resourcePath .']##');

    }


    /**
     * This is called upon `wp_loaded` - action hook useful for 
     * intercepting $_GET or $_POST triggers which 
     * fires after WordPress has finished loading 
     * but before any headers are sent.
     * 
     * Most of WP is loaded at this stage and 
     * the user is authenticated.
     * WP_Query $q
     */
    public function hook_wp_init ( ) {

        $postID  = url_to_postid( $_SERVER['REQUEST_URI'] );
        $userObj = wp_get_current_user();#$userObj->roles[0]

    }


    /**
     * Load the required dependencies for this plugin.
     *
     * Include the following files that make up the plugin:
     *
     * - WP_NTFS_Browser_Loader. Orchestrates the hooks of the plugin.
     * - WP_NTFS_Browser_i18n. Defines internationalization functionality.
     * - WP_NTFS_Browser_Admin. Defines all hooks for the admin area.
     * - WP_NTFS_Browser_Public. Defines all hooks for the public side of the site.
     *
     * Create an instance of the loader which will be used to register the hooks
     * with WordPress.
     *
     * @since    0.1
     * @access   private
     */
    private function load_dependencies() {

        /** The class responsible for orchestrating the actions and filters of the core plugin */
        require_once WPNTFS__PLUGIN_DIR .'includes/class-wp-ntfs-browser-loader.php';

        /** The class responsible for defining all actions that occur in the admin area */
        require_once WPNTFS__PLUGIN_DIR .'includes/class-wp-ntfs-browser-admin.php';

        /** The class responsible for defining all actions that occur in the public-facing side of the site */
        require_once WPNTFS__PLUGIN_DIR .'includes/class-wp-ntfs-browser-public.php';

        /** The class responsible for defining internationalization functionality of the plugin */
        #require_once WPNTFS__PLUGIN_DIR .'includes/class-wp-ntfs-browser-i18n.php';

        $this->plugin_loader = new WP_NTFS_Browser_Loader();
    }

    /**
     * Define the locale for this plugin for internationalization.
     *
     * Uses the WP_NTFS_Browser_i18n class in order to set the 
     * domain and to register the hook with WordPress.
     *
     * @todo    Everything!
     * @access  private
     */
    private function set_locale() {
        #$plugin_i18n = new WP_NTFS_Browser_i18n();
        #$this->plugin_loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );
    }

    /**
     * Register all of the hooks related to the admin area functionality
     * of the plugin.
     *
     * @access   private
     */
    private function define_admin_hooks() {
        #$this->plugin_loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

        // instantiate admin class:
        $plugin_admin = new WP_NTFS_Browser_Admin( $this->get_plugin_name(), $this->get_version() );

        /** add link and define the administration / config options page */
        $this->plugin_loader->add_action('admin_menu', $plugin_admin, 'register_admin_page');

        /** define settings / config options on admin_init hook */
        $this->plugin_loader->add_action('admin_init', $plugin_admin, 'admin_init');
        #$this->plugin_loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_styles'); #'enqueue_scripts'
    }

    /**
     * Register all of the hooks related to the public-facing 
     * functionality of the plugin, like SHORTCODES.
     *
     * @since    0.1
     * @access   private
     */
    private function define_public_hooks() {
        add_shortcode( 'ntfs_browse',       array( $this, 'doBrowse' ) );
        add_shortcode( 'ntfs_search_form',  array( $this, 'showSearchForm' ) );
    }

    /**
     * Run the loader to execute all of the hooks with WordPress.
     *
     * @since    0.1
     */
    public function run() {
        $this->plugin_loader->run();
    }

    /**
     * The name of the plugin used to uniquely identify it within 
     * the context of WordPress and to define 
     * internationalization functionality.
     *
     * @since     0.1
     * @return    string    The name of the plugin.
     */
    public function get_plugin_name() {
        return $this->plugin_name;
    }

    /**
     * The reference to the class that orchestrates the hooks with the plugin.
     *
     * @since     0.1
     * @return    WP_NTFS_Browser_Loader    Orchestrates the hooks of the plugin.
     */
    public function get_loader() {
        return $this->plugin_loader;
    }

    /**
     * Retrieve the version number of the plugin.
     *
     * @since     0.1
     * @return    string    The version number of the plugin.
     */
    public function get_version() {
        return $this->version;
    }

    /**
     * Method to set the correct directory separator value.
     * 
     * @since     0.1
     * 
     * @todo Maybe there's a way to detect the environment 
     * (ie Win or UNIX) and set this value automatically?
     */
    public function setDirSeparator ($isWFS = false) {

        if ($isWFS)
        {
            $this->dirSeparator = '\\';
            $this->isWinFS = true;
        }
        else { 
            $this->dirSeparator = DIRECTORY_SEPARATOR;
            $this->isWinFS = false;
        }

        /*
        ## Can I detect FS automatically?
        if ( isWinFS() )
            $this->dirSeparator = '\\';

        if ( isUnixFS() )
            $this->dirSeparator = '/';
        */
    }

    /**
     * Method to return the directory separator
     * @since     0.1
     */
    public function getDirSeparator () {
        return $this->dirSeparator;
    }

    public function isWindows() {
        /**
         * no support for M$ Windows OS :(
         */
        return FALSE;
    }

    /**
     * Method to check if path is a DIRectory
     * @since     0.1
     */
    public function isDir ($filePath = NULL) {

        if (!$filePath)
            $filePath = $this->resourcePath;

        if ( is_dir($filePath) )
            return TRUE;

        return FALSE;
    }

    /**
     * Method to check if the path is a file
     * @since     0.1
     */
    public function isFile ($filePath = NULL) {

        if (!$filePath)
            $filePath = $this->resourcePath;

        if ( file_exists($filePath) )
            return TRUE;

        return FALSE;
    }

    /**
     * Method to return the contents of a 
     * directory in a PHP array.
     * 
     * @since     0.1
     */
    public function getDirContents ($filePath = NULL) {

        if (!$filePath)
            $filePath = $this->resourcePath;

        $dir_contents = scandir($filePath);
        if (count($dir_contents) > 0)
        {
            foreach ($dir_contents as $dirK => $dirV)
            {
                # don't show hidden files:
                if ( !$this->showHiddenFiles && $dirV[0] == '.')
                    unset($dir_contents[$dirK]);

                # don't show hidden extensions:
                $thisExt = $this->getFileExt($dirV);
                if ( in_array($thisExt, $this->hideFileExt) || in_array($dirV, $this->hideFileExt))
                    unset($dir_contents[$dirK]);
            }

            # remove certain values from contents list:
            #if (($key = array_search('.env', $dir_contents)) !== false) 
            #    unset($dir_contents[$key]);
        }
        else {
            $dir_contents = ['empty'];
        }

        return $dir_contents;
    }

    /**
     * Method to return the HTML formatted list 
     * of a directory's contents.
     * 
     * @since   0.1
     * @var     $filePath   browsing path 
     */
    public function listDirContents ($filePath = NULL) {

        if (!$filePath)
            $filePath = $this->resourcePath;

        $htmlList = "".
                    "<p>Browsing &quot;<span>$filePath</span>&quot; repository:</p>".
                    "<ol>";
        foreach ($this->getDirContents($filePath) as $contents_name)
        {
                $htmlList .= "<li><a href='?path=". urlencode($this->resourceURL . $this->dirSeparator .$contents_name) ."'>$contents_name</a></li>";
        }
        $htmlList .= "</ol>";

        return $htmlList;
    }

    /**
     * Calls methods that fetch directory contents 
     * and return the HTML formatted list.
     * 
     * @since   0.1
     * @var     $filePath   browsing path 
     */
    public function doBrowse ($filePath = NULL) {

        if (!$filePath)
            $filePath = $this->resourcePath;

        if ($this->isDir($filePath) )
        {
            # include the search form when browsing:
            $this->showSearchForm();

            # list the contents of the directory:
            echo "<div id='". WPNTFS__PLUGIN_PREFIX ."container' class='".WPNTFS__PLUGIN_PREFIX."container'>".
                    "<a href='javascript:history.back();' class='". WPNTFS__PLUGIN_PREFIX ."backlink'>Go Back</a> ". 
                    $this->listDirContents() .
                  "</div>";

            # should it ever come to this?
            if (!$this->isDir($filePath))
                echo "<p>ERROR: Cannot read ". ($filePath ?: 'root') .".</p>";
        }
        else {
            /**
             * Viewing / downloading files is captured with the
             * init HOOK in method wp_init() so as to have 
             * better control over headers and buffers.
             */
            echo $this->getFileContents($filePath);
            #die ("no no no...");
        }

    }

    /**
     * Displays the seach form incl search 
     * results if POST request detected.
     * The form itself is external file.
     *  
     * @since     0.1
     * @return    string  HTML formatted form
     */
    public function showSearchForm () {

        # default search form is in include file:
        $searchForm = "";#get_header('single');

         ob_start(); // start output buffer
            include_once(WPNTFS__PLUGIN_DIR .'public/search_form.php');
            $searchForm .= ob_get_contents(); // fetch the contents of the buffer
        ob_end_clean(); // cleanup the buffer

        if ($_POST)
        {
            # sanitize user input:
            $searchKeyword = htmlentities(@$_POST['ntfs_keyword_search']);
            $searchType    = htmlentities(@$_POST['ntfs_type_search']);
            $searchAtts    = array(
                                'keyword'   => $searchKeyword, 
                                'type'      => $searchType,
                                'path'      => $this->resourcePath,
                                );

            # pass variable to search method in array:
            $searchForm   .= $this->getSearchResults($searchAtts);
        }

        echo $searchForm;
    }

    /**
     * private method to perform LDAP search 
     * using user filters passed in an array
     * 
     * @since   0.1
     * @param   array   search atributes array
     * @return  string  an HTML list in DIV container
     */
    private function getSearchResults ($attr = array()) {

        if (!count($attr) > 1)
            return false;

        global $wp;

        /** perform the actual search */
        $searchResults    = $this->doSearch($attr);
        #echo "<pre>$searchPattern Searching for &quot;{$attr['keyword']}&quot; in {$attr['path']} produced:". print_r($searchResults, true) ."</pre>";

        # define HTML container 
        $resultsList      = "<div class='ad_search_results'>";

        if (count(@$searchResults) > 0 && @$searchResults['count'] > 0)
        {
            $resultsList .= "<p>Found ".$searchResults['count'] ." results in {$attr['path']}:</p>";
            $resultsList .= "<ol class='ad_search_results'>";
            foreach ($searchResults['results'] as $searchResultItem)
            {
                $searchResultItem = array_filter($searchResultItem);
                $resultsList .= "<li>".
                                #home_url( $wp->request )
                                "<a target='_blank' href='". get_page_link( ) ."?path=". urlencode(@$searchResultItem['file_path']) ."'>". @$searchResultItem['file_name'] ."</a> ".
                                (!$searchResultItem['file_readable'] ? '(not readable)' : '') ."<br />".
                                "(path: {$searchResultItem['file_path']})".
                                "</li>";
            }
            $resultsList .= "</ol>";
        }
        else {
            $resultsList .= "<p>Nothing found matching &quot;".$attr['keyword'] ."&quot;.</p>";
        }

        # close the HTML container:
        $resultsList     .= "</div>";

        return $resultsList;
    }

    /**
     * Calls the recursive search method 
     * and takes the search results and 
     * formats them in a user friendly 
     * way.
     * @since   0.1
     * 
     * @var     array   search attributes (e.g. array(keyword, search type, path); )
     * @return  array
     */
    public function doSearch($attr) {

        # define blank results array:
        $searchResults              = array('count' => 0, 'results' => array());

        # search recursevly:
        if (!$attr['path'])
        {
            $attr['path'] = $this->resourcePath;
            $searchPath   = $this->resourcePath;
            #$searchPath  = ($attr['path'] ?: $this->resourcePath);
        }
        else {
            $searchPath   = $attr['path'];
        }
        $searchResultsTree          = $this->doSearchRecursive($attr);
        $searchResults['count']     = count($searchResultsTree);

        #echo "<pre>Doing recursive search of &quot;{$attr['keyword']}&quot; in $searchPath produced:". print_r($searchResultsTree, true) ."</pre>";
        foreach ($searchResultsTree as $searchResultPath)
        {
            if ( !in_array($searchResultPath, array('.', '..', '') ) )
            {
                # add file to as results entry
                #if ( $this->isFile($searchResultPath) )
                    $searchResults['results'][] = array(
                                                    'file_path'     => $this->getSafePath($searchResultPath),
                                                    'file_name'     => $this->getFileName($searchResultPath),
                                                    'file_ext'      => $this->getFileExt($searchResultPath),
                                                    'file_mime'     => $this->getFileMIME($searchResultPath),
                                                    'file_readable' => $this->canRead($searchResultPath),
                                                    );
            }
        }
        return $searchResults;
    }

    /**
     * Method to perform a recursive search (from 
     * where the user is).
     * @since   0.1
     * 
     * @var     array   attributes (e.g. array(keyword, search type, path); )
     * @return  array
     */
    private function doSearchRecursive($attr) {

        # define blank search results array:
        $searchResults              = array();
        #echo "<pre>recursively searching attr:". print_r($attr, true) ."</pre>";

        # define search parameters:
        $searchPath                 = ($attr['path'] ?: $this->resourcePath);
        $searchKeyword              = htmlentities(@$attr['keyword']);
            $searchKeyword          = str_ireplace(' ', '*', $searchKeyword);
        $searchType                 = @$attr['type'];
        switch ($searchType)
        {
            case 'begining':
                $searchPattern      = "/$searchKeyword*";
                break;
            case 'ending':
                $searchPattern      = "/*$searchKeyword";
                break;
            default:
                # containing:
                $searchPattern      = "/*$searchKeyword*";
                break;
        }

        # search for pattern and return paths:
        $searchResultsTree          = glob($searchPath . $searchPattern, GLOB_BRACE);
        foreach ($searchResultsTree as $searchSubResult)
        {
            $searchResults[]        = $searchSubResult; #matching results in current location
        }
        /*echo "<pre>1) ". 
                "recursively searching &quot;<strong>{$attr['keyword']}</strong>&quot; ($searchType) in &quot;$searchPath&quot;.".
                "<br />results tree:".   print_r($searchResultsTree, true) .
                "<br />results:".   print_r($searchResults, true) .
                "<br />attr:".      print_r($attr, true) .
                "<br />searchPattern:$searchPattern".
                "<br />dir contents:". print_r($this->getDirContents($searchPath), true) .
                "</pre>";*/

        # recurse into sub directories if current path has any:
        foreach ($this->getDirContents($searchPath) as $contents_name)
        {
            $subSearchResults       = array();
            $subPath                = $searchPath . $this->dirSeparator . $contents_name;

            if ( $this->isDir($subPath) )# && !in_array($contents_name, $this->hideFileExt) )
            {
                $recurseSearchAttr  = array(
                                        'keyword'   => $attr['keyword'],
                                        'path'      => $subPath,
                                        'type'      => @$attr['type'],
                                        #'x'         => '?'. $searchPath,
                                        );
                # recursively run itself when we get to a subfolder:
                $subSearchResults   = $this->doSearchRecursive($recurseSearchAttr);

                # debugging info:
                /*echo "<pre>2) recursing into {$recurseSearchAttr['path']}". 
                        "<br />recursing &quot;<strong>{$recurseSearchAttr['keyword']}</strong>&quot; ($searchType) in &quot;$subPath&quot;.".
                        "<br />results:".   print_r($subSearchResults, true) .
                        "<br />attr:".      print_r($recurseSearchAttr, true) .
                        "<br />dir contents:". print_r($this->getDirContents($subPath), true) .
                        "</pre>";*/

                # merge search results:
                $searchResults      = array_merge ($searchResults, $subSearchResults);
           }
           else {
                /*echo "<pre>skipping $subPath cause not dir;</pre>";*/
           }
        }

        # return results:
        return $searchResults;
    }

    /**
     * Method to return a path without the 'root' 
     * path (e.g. "/var/secret-root/").
     * @since   0.1
     */
    public function getSafePath ($filePath = NULL) {

        if (!$filePath)
            $filePath = $this->resourcePath;

        if (!$filePath)
            return false;

        # remove doubled up separators:
        $filePath =  str_ireplace($this->dirSeparator.$this->dirSeparator, $this->dirSeparator, $filePath);

        if ($this->resourceRoot != '/' && $this->resourceRoot != '')
            $filePath =  str_ireplace( $this->resourceRoot, '', $filePath ) ;

        if ($this->sharePath != '/' && $this->sharePath != '')
            $filePath =  str_ireplace( $this->sharePath, '', $filePath ) ;

        return $filePath;
    }

    /**
     * Method to return the extension of a file
     * @since   0.1
     */
    public function getFileExt ($filePath = NULL) {

        if (!$filePath)
            $filePath = $this->resourcePath;

        return pathinfo($filePath, PATHINFO_EXTENSION);
    }

    /**
     * Method to return the MIME type of a file
     * @since   0.1
     */
    public function getFileMIME ($filePath = NULL) {

        if (!$filePath)
            $filePath = $this->resourcePath;

        return mime_content_type($filePath);
    }

    /**
     * Method to return the filename
     * @since   0.1
     */
    public function getFileName ($filePath = NULL) {

        if (!$filePath)
            $filePath = $this->resourcePath;

        return basename($filePath);
    }

    /**
     * Method to return the contents of a 
     * file for display in the browser or
     * offer to download.
     * @since   0.1
     */
    public function getFileContents($filePath = NULL) {

        if (!$filePath)
            $filePath = $this->resourcePath;

        #die(debug_backtrace()[1]['function'] ."||filePath:$filePath||". $this->getFileExt() ."@". $this->headerMIMEs[$this->getFileExt()] ."//". @filesize($filePath) ."[". $this->canView($filePath) ."?][". $this->canDownload($filePath) ."?]");
        if ($this->canView($filePath))
        {
            ob_start();
                header('Content-Type: ' . $this->headerMIMEs[$this->getFileExt()]);
                header('Content-Length: ' . filesize($filePath));
                echo (file_get_contents($filePath));
                $embedHTML .= ob_get_contents(); // fetch the contents of the buffer
            ob_end_clean(); // cleanup the buffer
            echo ($embedHTML);
        }
        #die(debug_backtrace()[1]['function'] ."filePath:$filePath//". $this->getFileExt() ."@". $this->headerMIMEs[$this->getFileExt()] ."//". @filesize($filePath) ."[". $this->canView($filePath) ."?][". $this->canDownload($filePath) ."?]");

        if ($this->canDownload($filePath))
        {
            # prevent browsers from caching the download:
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Type: application/force-download');
            header('Content-Type: application/download');
            header('Content-Disposition: attachment; filename="'.basename($filePath).'"');#filename='. urlencode($filePath)
            header('Expires: 0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($filePath));

            echo readfile($filePath);
        }

        # catch anything else:
        echo "&quot;". $this->resourceURL ."&quot; is unavailable";
    }

    /**
     * Check if resource (ie folder) is readable
     * @since   0.1
     */
    public function canRead($filePath = NULL) {

        if (!$filePath)
            $filePath = $this->resourcePath;

        # prevent access to sys files
        #if ($filePath[0] == '.')
        if (in_array($filePath, $this->hideFileExt) )
            return false;
            #return header('HTTP/1.0 666 Denied');

        if (is_readable($filePath))
            return TRUE;

        return FALSE;
    }

    /**
     * Check if file type is allowed to view
     * @since   0.1
     */
    public function canView($filePath = NULL) {

        #die("<pre>$filePath||".$this->headerMIMEs[$this->getFileExt()] ."#". $this->getFileExt().":". print_r($this->headerMIMEs, true)."</pre>");
        if (!$filePath)
            $filePath = $this->resourcePath;

        # prevent access to sys files
        if (in_array( $filePath, $this->hideFileExt ) )
            return false;

        if ( isset($this->headerMIMEs[$this->getFileExt()]) && $this->headerMIMEs[$this->getFileExt()] != '')
            return true;

        return false;
    }

    /**
     * Check if file type is allowed to download
     * @since   0.1
     */
    public function canDownload($filePath = NULL) {

        if (!$filePath)
            $filePath = $this->resourcePath;

        # prevent access to sys files
        if (in_array($filePath, $this->hideFileExt) )
            return false;

        if (!in_array( $this->getFileExt(), $this->hideFileExt) )
            return true;

        return false;
    }


    /**
     * get MIME list from Apache official
     * @since   0.1
     */
    public function getApacheMIMEs () {
        $mimes = array();

        // get each line and evaluate:
        foreach (@explode("\n", @file_get_contents('http://svn.apache.org/repos/asf/httpd/httpd/trunk/docs/conf/mime.types')) as $get_line)
        {
            if (
                isset($get_line[0])         # skip empty lines
                && $get_line[0] !== '#'     # skip lines commented out
                && preg_match_all('#([^\s]+)#', $get_line, $out) # split lines on consecutive whitespace into $out array
                && isset($out[1]) 
                && ($c = count($out[1])) > 1
                )
                {
                    for ($i = 1; $i < $c; $i++)
                    {
                        $mimes[$out[1][$i]] = $out[1][0];
                    }
                }
        }
        return $mimes;
    }


    /**
     * Public method to test the paths and 
     * web user's to access
     *
     * @param  string  $username
     * @param  string  $password
     * @return array   $dir-contents
     */
    public function test_conn() {

        $testpath    = $this->resourceRoot . $this->dirSeparator . $this->sharePath;
            $testpath=  str_ireplace($this->dirSeparator.$this->dirSeparator, $this->dirSeparator, $testpath);
        $testresults = "browsing $testpath:". $this->listDirContents($testpath);

        return $testresults;

    }

  }
}